import subprocess

with open("hosts_and_ports.txt") as hp_fh:
	
    hp_contents = hp_fh.readlines()
    for hp_pair in hp_contents:
        host_port = hp_pair.split(":")
        host = host_port[0]
        port = host_port[1]
			
        with open("commands.txt") as fh:
            completed = subprocess.run("C:\\WINDOWS\\System32\\OpenSSH\\ssh.exe {}@127.0.0.1 -p {}".format(host, port), capture_output=True, text=True, shell=True, stdin=fh)