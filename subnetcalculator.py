def ask_for_number_sequence(message):
    inputGebruiker = input(message)
	
    stringInput = inputGebruiker.split(".")
    adress = []
	
    for item in stringInput:
	    number = int(item)
	    adress.append(number)
	
    return adress
	


def is_valid_ip_addres(numberlist):
	
    if len(numberlist) != 4:
        return False
    
    for number in numberlist:
        
        if number < 0 or number > 255:
            return False
    
    return True
    
def is_valid_netmask(numberlist):
    binary_netmask = ""
    bitsList = []
    
    for number in numberlist:
        
        if number != int(number):
            return False
    
        bitsList.append(f"{number:08b}")
    
    binary_netmask.join(bitsList)
    
    checking_ones = True
    
    for c in binary_netmask:
        
        if checking_ones and c == 0:
            checking_ones = False
        elif c == 1:
            print("Geen geldig netmasker")
            return False
    
    return True
    
def one_bits_in_netmask(netmask):
    binary_netmask = ""
    bitsList = []
    
    for number in netmask:
        
        if number != int(number):
            return False
    
        bitsList.append(f"{number:08b}")
    
    binary_netmask = binary_netmask.join(bitsList)
    
    counter = 0
    
    for c in binary_netmask:
        
        if c == "1":
            counter += 1
    
    return counter
    
def apply_network_mask(host_address, netmask):
    network_address = []
    
    for i in range(0, 4):
        network_address.append(int((f"{host_address[i] & netmask[i]:b}"),2))
    
    return network_address
    
def netmask_to_wildcard_mask(netmask):
    
    wildcard_mask = []
    
    for number in netmask:
        converted = ""
        list = []
        byte = str(f"{number:08b}")
    
        for b in byte:
            if b == "0":
                list.append("1")
            elif b == "1":
                list.append("0")
        
        
        converted = converted.join(list)
        wildcard_mask.append(int(converted,2))
    
    return wildcard_mask
    
def get_broadcast_address(network_address, wildcard_mask):
    
    broadcast_address = []
    
    for i in range(0, 4):
        broadcast_address.append(int(f"{network_address[i] | wildcard_mask[i]:b}",2))
    
    return broadcast_address
    
def prefix_lenght_to_max_hosts(subnetlenght):
    
    hosts = (2 ** (32 - subnetlenght)) - 2
    return hosts
    
ipadres = ask_for_number_sequence("Wat is het IP-adres?\n")
subnetmask = ask_for_number_sequence("Wat is het subnetmask?\n")

if is_valid_ip_addres and is_valid_netmask:
    print("IP-adres en subnetmask zijn geldig")
else:
    sys.exit(0)

print("De lengte van het subnetmasker is ",one_bits_in_netmask(subnetmask), "\n")
print("Het adres van het subnet is ",apply_network_mask(ipadres, subnetmask),"\n")
print("Het wildcardmasker is " , netmask_to_wildcard_mask(subnetmask), "\n")
print("Het broadcastadres is ",get_broadcast_address(apply_network_mask(ipadres, subnetmask),netmask_to_wildcard_mask(subnetmask)),"\n")
print("Het maximaal aantal hosts op dit subnet is ",prefix_lenght_to_max_hosts(one_bits_in_netmask(subnetmask)), "\n")

